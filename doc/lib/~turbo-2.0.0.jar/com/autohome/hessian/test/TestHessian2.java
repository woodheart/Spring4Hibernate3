// Decompiled by Jad v1.5.8e2. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://kpdus.tripod.com/jad.html
// Decompiler options: packimports(3) fieldsfirst ansi space 
// Source File Name:   TestHessian2.java

package com.autohome.hessian.test;


public interface TestHessian2
{

	public abstract void methodNull();

	public abstract void replyNull();

	public abstract Object replyTrue();

	public abstract Object replyFalse();

	public abstract int replyInt_0();

	public abstract int replyInt_1();

	public abstract int replyInt_47();

	public abstract int replyInt_m16();

	public abstract int replyInt_0x30();

	public abstract int replyInt_0x7ff();

	public abstract int replyInt_m17();

	public abstract int replyInt_m0x800();

	public abstract int replyInt_0x800();

	public abstract int replyInt_0x3ffff();

	public abstract int replyInt_m0x801();

	public abstract int replyInt_m0x40000();

	public abstract int replyInt_0x40000();

	public abstract int replyInt_0x7fffffff();

	public abstract int replyInt_m0x40001();

	public abstract int replyInt_m0x80000000();

	public abstract long replyLong_0();

	public abstract long replyLong_1();

	public abstract long replyLong_15();

	public abstract long replyLong_m8();

	public abstract long replyLong_0x10();

	public abstract long replyLong_0x7ff();

	public abstract long replyLong_m9();

	public abstract long replyLong_m0x800();

	public abstract long replyLong_0x800();

	public abstract long replyLong_0x3ffff();

	public abstract long replyLong_m0x801();

	public abstract long replyLong_m0x40000();

	public abstract long replyLong_0x40000();

	public abstract long replyLong_0x7fffffff();

	public abstract long replyLong_m0x40001();

	public abstract long replyLong_m0x80000000();

	public abstract long replyLong_0x80000000();

	public abstract long replyLong_m0x80000001();

	public abstract double replyDouble_0_0();

	public abstract double replyDouble_1_0();

	public abstract double replyDouble_2_0();

	public abstract double replyDouble_127_0();

	public abstract double replyDouble_m128_0();

	public abstract double replyDouble_128_0();

	public abstract double replyDouble_m129_0();

	public abstract double replyDouble_32767_0();

	public abstract double replyDouble_m32768_0();

	public abstract double replyDouble_0_001();

	public abstract double replyDouble_m0_001();

	public abstract double replyDouble_65_536();

	public abstract double replyDouble_3_14159();

	public abstract Object replyDate_0();

	public abstract Object replyDate_1();

	public abstract Object replyDate_2();

	public abstract String replyString_0();

	public abstract String replyString_null();

	public abstract String replyString_1();

	public abstract String replyString_31();

	public abstract String replyString_32();

	public abstract String replyString_1023();

	public abstract String replyString_1024();

	public abstract String replyString_65536();

	public abstract Object replyBinary_0();

	public abstract Object replyBinary_null();

	public abstract Object replyBinary_1();

	public abstract Object replyBinary_15();

	public abstract Object replyBinary_16();

	public abstract Object replyBinary_1023();

	public abstract Object replyBinary_1024();

	public abstract Object replyBinary_65536();

	public abstract Object replyUntypedFixedList_0();

	public abstract Object replyUntypedFixedList_1();

	public abstract Object replyUntypedFixedList_7();

	public abstract Object replyUntypedFixedList_8();

	public abstract Object replyTypedFixedList_0();

	public abstract Object replyTypedFixedList_1();

	public abstract Object replyTypedFixedList_7();

	public abstract Object replyTypedFixedList_8();

	public abstract Object replyUntypedMap_0();

	public abstract Object replyUntypedMap_1();

	public abstract Object replyUntypedMap_2();

	public abstract Object replyUntypedMap_3();

	public abstract Object replyTypedMap_0();

	public abstract Object replyTypedMap_1();

	public abstract Object replyTypedMap_2();

	public abstract Object replyTypedMap_3();

	public abstract Object replyObject_0();

	public abstract Object replyObject_16();

	public abstract Object replyObject_1();

	public abstract Object replyObject_2();

	public abstract Object replyObject_2a();

	public abstract Object replyObject_2b();

	public abstract Object replyObject_3();

	public abstract Object argNull(Object obj);

	public abstract Object argTrue(Object obj);

	public abstract Object argFalse(Object obj);

	public abstract Object argInt_0(Object obj);

	public abstract Object argInt_1(Object obj);

	public abstract Object argInt_47(Object obj);

	public abstract Object argInt_m16(Object obj);

	public abstract Object argInt_0x30(Object obj);

	public abstract Object argInt_0x7ff(Object obj);

	public abstract Object argInt_m17(Object obj);

	public abstract Object argInt_m0x800(Object obj);

	public abstract Object argInt_0x800(Object obj);

	public abstract Object argInt_0x3ffff(Object obj);

	public abstract Object argInt_m0x801(Object obj);

	public abstract Object argInt_m0x40000(Object obj);

	public abstract Object argInt_0x40000(Object obj);

	public abstract Object argInt_0x7fffffff(Object obj);

	public abstract Object argInt_m0x40001(Object obj);

	public abstract Object argInt_m0x80000000(Object obj);

	public abstract Object argLong_0(Object obj);

	public abstract Object argLong_1(Object obj);

	public abstract Object argLong_15(Object obj);

	public abstract Object argLong_m8(Object obj);

	public abstract Object argLong_0x10(Object obj);

	public abstract Object argLong_0x7ff(Object obj);

	public abstract Object argLong_m9(Object obj);

	public abstract Object argLong_m0x800(Object obj);

	public abstract Object argLong_0x800(Object obj);

	public abstract Object argLong_0x3ffff(Object obj);

	public abstract Object argLong_m0x801(Object obj);

	public abstract Object argLong_m0x40000(Object obj);

	public abstract Object argLong_0x40000(Object obj);

	public abstract Object argLong_0x7fffffff(Object obj);

	public abstract Object argLong_m0x40001(Object obj);

	public abstract Object argLong_m0x80000000(Object obj);

	public abstract Object argLong_0x80000000(Object obj);

	public abstract Object argLong_m0x80000001(Object obj);

	public abstract Object argDouble_0_0(Object obj);

	public abstract Object argDouble_1_0(Object obj);

	public abstract Object argDouble_2_0(Object obj);

	public abstract Object argDouble_127_0(Object obj);

	public abstract Object argDouble_m128_0(Object obj);

	public abstract Object argDouble_128_0(Object obj);

	public abstract Object argDouble_m129_0(Object obj);

	public abstract Object argDouble_32767_0(Object obj);

	public abstract Object argDouble_m32768_0(Object obj);

	public abstract Object argDouble_0_001(Object obj);

	public abstract Object argDouble_m0_001(Object obj);

	public abstract Object argDouble_65_536(Object obj);

	public abstract Object argDouble_3_14159(Object obj);

	public abstract Object argDate_0(Object obj);

	public abstract Object argDate_1(Object obj);

	public abstract Object argDate_2(Object obj);

	public abstract Object argString_0(Object obj);

	public abstract Object argString_1(Object obj);

	public abstract Object argString_31(Object obj);

	public abstract Object argString_32(Object obj);

	public abstract Object argString_1023(Object obj);

	public abstract Object argString_1024(Object obj);

	public abstract Object argString_65536(Object obj);

	public abstract Object argBinary_0(Object obj);

	public abstract Object argBinary_1(Object obj);

	public abstract Object argBinary_15(Object obj);

	public abstract Object argBinary_16(Object obj);

	public abstract Object argBinary_1023(Object obj);

	public abstract Object argBinary_1024(Object obj);

	public abstract Object argBinary_65536(Object obj);

	public abstract Object argUntypedFixedList_0(Object obj);

	public abstract Object argUntypedFixedList_1(Object obj);

	public abstract Object argUntypedFixedList_7(Object obj);

	public abstract Object argUntypedFixedList_8(Object obj);

	public abstract Object argTypedFixedList_0(Object obj);

	public abstract Object argTypedFixedList_1(Object obj);

	public abstract Object argTypedFixedList_7(Object obj);

	public abstract Object argTypedFixedList_8(Object obj);

	public abstract Object argUntypedMap_0(Object obj);

	public abstract Object argUntypedMap_1(Object obj);

	public abstract Object argUntypedMap_2(Object obj);

	public abstract Object argUntypedMap_3(Object obj);

	public abstract Object argTypedMap_0(Object obj);

	public abstract Object argTypedMap_1(Object obj);

	public abstract Object argTypedMap_2(Object obj);

	public abstract Object argTypedMap_3(Object obj);

	public abstract Object argObject_0(Object obj);

	public abstract Object argObject_16(Object obj);

	public abstract Object argObject_1(Object obj);

	public abstract Object argObject_2(Object obj);

	public abstract Object argObject_2a(Object obj);

	public abstract Object argObject_2b(Object obj);

	public abstract Object argObject_3(Object obj);
}
