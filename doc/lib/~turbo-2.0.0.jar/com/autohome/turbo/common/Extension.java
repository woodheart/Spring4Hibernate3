// Decompiled by Jad v1.5.8e2. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://kpdus.tripod.com/jad.html
// Decompiler options: packimports(3) fieldsfirst ansi space 
// Source File Name:   Extension.java

package com.autohome.turbo.common;

import java.lang.annotation.Annotation;

/**
 * @deprecated Interface Extension is deprecated
 */

public interface Extension
	extends Annotation
{

	/**
	 * @deprecated Method value is deprecated
	 */

	public abstract String value();
}
